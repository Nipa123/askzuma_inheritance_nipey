package Askzuma_pkg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;

public class SignIn {
 public WebDriver driver;
  @BeforeClass
  public void beforeClass() throws InterruptedException {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\Integrated\\OneDrive\\Desktop\\Selenium Driver\\chromedriver.exe");
	  driver = new ChromeDriver();
	  driver.manage().window().maximize();
	  driver.get("https://askzuma.com/");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
  }

  @Test(priority = 0)
  public void EmailPwBlank() throws InterruptedException {
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	  String msge = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	  String emsge ="Required";
	  Assert.assertEquals(msge, emsge);
	  Thread.sleep(3000);
	  
	  String msgpw = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
	  String emsgpw = "Required";
	  Assert.assertEquals(msgpw, emsgpw);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
  }
  
  @Test(priority = 1)
  public void EmailBlank() throws InterruptedException {
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Password")).sendKeys("12345678");
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	  String msge = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	  String emsge ="Required";
	  Assert.assertEquals(msge, emsge);
	  Thread.sleep(3000);
	  
	  
	  
	  driver.navigate().refresh();
  }
  
  @Test(priority = 2)
  public void PWBlank() throws InterruptedException {
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Email")).sendKeys("ankur1.manish@gmail.com");
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	 	  
	  String msgpw = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[2]/span")).getText();
	  String emsgpw = "Required";
	  Assert.assertEquals(msgpw, emsgpw);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
  }
  
  @Test(priority = 3)
  public void InvalidEmailPW() throws InterruptedException {
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Email")).sendKeys("ankur1.manish@");
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Password")).sendKeys("!@#$%^&");
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	 	  
	  String msge = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	  String emsge = "Invalid format";
	  Assert.assertEquals(msge, emsge);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
  }
  
  @Test(priority = 4)
  public void InvalidEmail() throws InterruptedException {
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Email")).sendKeys("ankur1.manish@");
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Password")).sendKeys("12345678");
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	 	  
	  String msge = driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/div[1]/div[1]/span")).getText();
	  String emsge = "Invalid format";
	  Assert.assertEquals(msge, emsge);
	  Thread.sleep(3000);
	  
	  driver.navigate().refresh();
  }
  
  @Test(priority = 5)
  public void InvalidPW() throws InterruptedException {
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Email")).sendKeys("ankur1.manish@gmail.com");
	  Thread.sleep(3000);
	  driver.findElement(By.id("Password")).sendKeys("!@#$%^&");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	  
	 	  
	  String msgpw = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
	  String emsgpw = "Invalid email or password.";
	  Assert.assertEquals(msgpw, emsgpw);
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
	  Thread.sleep(5000);
	  
	  driver.navigate().refresh();
  }

  @Test(priority = 6)
  public void ValidEmailPW() throws InterruptedException {
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul[2]/li[1]/a")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.id("Email")).sendKeys("ankur1.manish@gmail.com");
	  Thread.sleep(3000);
	  driver.findElement(By.id("Password")).sendKeys("12345678");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/div/div/form/footer/button")).click();
	  Thread.sleep(3000);
	 	  
  }

  
  @AfterClass
  public void afterClass() throws InterruptedException {
	 driver.findElement(By.xpath("/html/body/div[7]/header/div/nav/a")).click();
	 Thread.sleep(5000);
	 driver.close();
	  
  }

}
