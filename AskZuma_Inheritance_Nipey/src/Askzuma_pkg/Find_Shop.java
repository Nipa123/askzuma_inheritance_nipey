package Askzuma_pkg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Find_Shop extends Home_Page {
 
  @BeforeTest
  public void beforeTest() {
  }

  @Test(priority = 9)
  public void DataIn_FindShop() throws InterruptedException {
	  
	  JavascriptExecutor js = (JavascriptExecutor)driver;
	  js.executeScript("scrollBy(0,-200)");
	  Thread.sleep(5000);
	  driver.navigate().refresh();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("/html/body/div[16]/header/div/nav/ul/li[2]/a")).click();
	  Thread.sleep(3000);      
	  driver.findElement(By.xpath("//*[@id=\"tbLocation\"]")).sendKeys("Canada");
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"btnSearchShops\"]")).click();
	  Thread.sleep(3000);
	  
	  String msg = driver.findElement(By.xpath("/html/body/div[7]/div[2]/main/div/section/section[2]/div[1]/ul[2]/li[1]/a")).getText();
	  String emsg = "ProColor Collision Toronto Junction";
	  Assert.assertEquals(msg, emsg);
	  Thread.sleep(3000);
  }
  
  @AfterTest
  public void afterTest() {
  }

}
