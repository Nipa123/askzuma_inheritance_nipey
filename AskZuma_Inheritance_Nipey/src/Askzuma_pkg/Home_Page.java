package Askzuma_pkg;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeTest;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;

public class Home_Page extends SignIn {
 
  @BeforeTest
  public void beforeTest() {
  }

  @Test(priority = 7)
  public void valueEnter() throws InterruptedException {
	  
	  driver.findElement(By.id("select2-chosen-1")).click();
	 // Select drp = new Select(driver.findElement(By.id("select2-chosen-1")));
	 // drp.selectByIndex(3);
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"select2-result-label-8\"]")).click();
	 Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("//*[@id=\"select2-chosen-46\"]")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"select2-result-label-52\"]")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("//*[@id=\"select2-chosen-90\"]")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"select2-result-label-91\"]")).click();
	  Thread.sleep(3000);
	  
	  driver.findElement(By.xpath("//*[@id=\"select2-chosen-92\"]")).click();
	  Thread.sleep(3000);
	  driver.findElement(By.xpath("//*[@id=\"select2-result-label-93\"]")).click();
	  Thread.sleep(3000);
  }
  
  @Test(priority = 8)
  public void scroll() throws InterruptedException {
	  JavascriptExecutor js = (JavascriptExecutor)driver;
	  js.executeScript("scrollBy(0,200)");
	  Thread.sleep(2000);
	driver.findElement(By.xpath("//*[@id=\"selectJob\"]")).click();
	Thread.sleep(3000);
  
	String msg = driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/div/div/div[2]")).getText();
	String emsg = "Please specify the the location.";
	
	Assert.assertEquals(msg, emsg);
	Thread.sleep(3000);
	
	driver.findElement(By.xpath("/html/body/div[12]/div/div/div/div/footer/button")).click();
	Thread.sleep(3000);
	//driver.navigate().refresh();
	//Thread.sleep(5000);
  }
  
  
  @AfterTest
  public void afterTest() {
  }

}
